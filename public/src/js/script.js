{
    let $ = global.$ || {};
    let jQuery = global.jQuery || {};

    $ = jQuery = require('jquery');

    global.$ = $;
    global.jQuery = jQuery;

	require('flexslider');

	// main slider
	$('.mainSlider').flexslider({
        useCSS: false,
        animation: "fade",
        slideshow: true,
        animationLoop: true,
        slideshowSpeed: 4000,
        animationSpeed: 700,
        pauseOnAction: false,
        pauseOnHover: false,
        directionNav: false,
        controlNav: true,
        manualControls: ".flex-control-nav li"
    });
    function flexdestroy(selector) {
        var el = $(selector);
        var elClean = el.clone();

        elClean.find('.flex-viewport').children().unwrap();
        elClean
            .removeClass('flexslider')
            .find('.clone, .flex-direction-nav, .flex-control-nav')
            .remove()
            .end()
            .find('*:not(a,.hover)').removeAttr('style').removeClass (function (index, css) {
            return (css.match (/\bflex\S+/g) || []).join(' ');
        });

        elClean.insertBefore(el);
        el.remove();
    }


    /*$('.referencesSlider').flexslider({
        slideshowSpeed: 2000,
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: true,
        itemWidth: $(window).width() > 900 ? $(window).width()/5 : $(window).width() < 500 ? $(window).width()/2 : $(window).width()/4,
        maxItems: 6,
        minItems: 2,
        itemMargin: 1,
        pauseOnHover: true,
        directionNav: false,
        move: 1
    });*/
    // flexdestroy('.referencesSlider');

	//main menu
    $('#mainMenu a').on('click', function (e) {
        e.preventDefault();
        var $this= $(this),
            href = $this.attr('href');

        $this.addClass('active').parent().siblings('li').find('a').removeClass('active');
        $('html, body').stop().animate({scrollTop: $('[data-block="'+href.substr(1)+'"]').offset().top}, 500);
        $('.megaMenuButton').trigger('click');
    });
    $('.headerLogo').on('click', function (e) {
        e.preventDefault();
        $('#mainMenu a[href="#life"]').addClass('active').parent().siblings('li').find('a').removeClass('active');
        $('html, body').stop().animate({scrollTop: $('[data-block="life"]').offset().top}, 500);
        $('.megaMenuButton').trigger('click');
    });
    
    $(window).on('scroll', function () {
        var $scrollTop = $(window).scrollTop(),
            $footer = $('footer');

        //Head menu active class begins
        $('[data-block]').each(function(){
            var $blckidwh = $(this).attr('data-block');
            if($scrollTop >= $("[data-block='"+$blckidwh+"']").offset().top-50){
                $('#mainMenu a').removeClass('active');
                $('#mainMenu a[href="#'+$blckidwh+'"]').addClass('active');

                if ($(window).width() < 768) {
                    if ($blckidwh === "about") {
                        $footer.hide();
                    } else {
                        $footer.show()
                    }
                }
            }
        });
    })

	// TAB BLOCK
    $('.tabNav a').on('click', function () {
        var $this = $(this),
            block = $this.data('block'),
            id = $this.data('id');

        $('[data-tab="'+block+'"]').find('li').removeClass('active').filter('[data-index="'+id+'"]').addClass('active');
        $this.parent().addClass('active').siblings().removeClass('active');

        if (id != 1) {
            $('.articleBlock').addClass('noBg');
        } else {
            $('.articleBlock').removeClass('noBg');
        }
    });

    $('.megaMenuButton').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.toggleClass('active');
        if ($this.hasClass('active')) {
            $('#mainMenu').addClass('active');
        } else {
            $('#mainMenu').removeClass('active');
        }
    });

    //günaydın mı
    var d = new Date(),
        time = d.getHours(),
        $time = $('#time');
    if (time>5 && time<10){
        $time.text('Günaydın');
    } else if (time>=10 && time<16) {
        $time.text('İyi Günler');
    } else {
        $time.text('İyi Akşamlar');
    }
};